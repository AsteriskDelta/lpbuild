#.SILENT:
BUILDSYS_PATH = $(realpath $(dir $(shell pwd)/../))
SPEC_DIR = $(realpath $(BUILDSYS_PATH)/specs)
SCRIPT_DIR = $(realpath $(BUILDSYS_PATH)/scripts)
TARGET_DIR = $(realpath $(BUILDSYS_PATH)/targets)
PLATFORM_DIR = $(realpath $(BUILDSYS_PATH)/platforms)

SPEC ?= $(MAKECMDGOALS)
PLATFORM ?= Desktop
BUILD_TYPE ?= debug
#Fool make into thinking we're Buildsys
MAKECMDGOALS += actual
PRINT_PFX ?=\033[2K

#TARGET_FAIL_CMD
ifdef TARGET_FAIL_CMD
	TARGET_FAIL_INVOKE = $(TARGET_FAIL_CMD)
	TARGET_FAIL_SUFFIX =
else
	TARGET_FAIL_INVOKE = echo
	TARGET_FAIL_SUFFIX = >/dev/null 2>&1 || true
endif

OUTPUT_DIR = $(SCRIPT_DIR)/.testSpec
#FAKE=$(shell echo "TARGETING $(SPEC_DIR)/$(SPEC)")
ifneq ($(wildcard $(SPEC_DIR)/$(SPEC).mk),)
ifndef SKIP_PLATFORM
include $(BUILDSYS_PATH)/platforms/$(PLATFORM).mk
endif
include $(SPEC_DIR)/$(SPEC).mk
endif

#TARGETS now defined, as per spec
#OUTPUT_FILES = $(TARGETS:%=$(OUTPUT_DIR)/%_$(VERSION_$$%).txt)
#OUTPRE_FAKE = $(foreach OUTPRE, $(TARGETS), $(eval $(VERSION_$(OUTPRE)) ?= trunk))
#ignore dirty hack to default to trunk
OUTPUT_FILES =$(foreach OUTPRE, $(TARGETS), $(OUTPUT_DIR)/$(OUTPRE)__$(PLATFORM)__$(firstword $(strip $(VERSION_$(OUTPRE)) trunk)).txt)

TESTSPEC_DIR:=.testSpec

default: all

prePrint:
	@echo -e "$(PRINT_PFX)[....]\t$(SPEC) for $(PLATFORM):$(BUILD_TYPE) {$(TARGETS)}"
#@echo "Targets: $(TARGETS)"
#@echo -e "PREPRINT: $(OUTPUT_FILES)"
	
mkOutputDir:
	@mkdir -p $(OUTPUT_DIR)
	@rm $(OUTPUT_DIR)/$(SPEC).fail 2>/dev/null || true

buildsysPrep:
	@echo "1" > $(OUTPUT_DIR)/$*.status
	@((set -o pipefail; make --no-print-directory -C $(BUILDSYS_PATH) $(PLATFORM) $(SPEC) do update preinstall 2>&1 | tee $(TESTSPEC_DIR)/$(ACTIVE_SPEC).pre || echo 0 > $(OUTPUT_DIR)/$*.status; ) ) ; cat $(TESTSPEC_DIR)/$(ACTIVE_SPEC).pre 2>/dev/null || ( cat $(TESTSPEC_DIR)/$(ACTIVE_SPEC).pre >> $(OUTPUT_DIR)/$(SPEC).fail || true; false)
	@if [[ "`cat $(OUTPUT_DIR)/$*.status 2> /dev/null || echo -1`" == "0" ]]; then\
		echo "Preinstall failed for $(P_STRING); bailing out";\
		false;\
	fi\
	
all: prePrint mkOutputDir buildsysPrep $(OUTPUT_FILES)
#@exit `cat $(OUTPUT_DIR)/$(SPEC).fail 2>/dev/null || echo 0`

$(OUTPUT_FILES): $(OUTPUT_DIR)/%.txt:
	$(eval ACTIVE_VERSION := $(lastword $(subst __, ,$*)))
	$(eval ACTIVE_TARGET := $(firstword $(subst __, ,$*)))
	$(eval ACTIVE_TARGET_PATH := $(TARGET_DIR)/$(ACTIVE_SPEC))
	$(eval ACTIVE_OWNER_PATH := $@.owns)
	$(eval P_STRING := $(ACTIVE_TARGET):$(ACTIVE_VERSION) for $(PLATFORM))
	@mkdir -p $(dir $(OUTPUT_DIR)/$*.status)
	
	@echo -ne "$(PRINT_PFX)\t[....]\t$(P_STRING) in progress...\r"
#Skip targets/versions that were already written OK, cleaned automatically
	set -o pipefail; (cat $(OUTPUT_DIR)/$*.status > /dev/null 2>&1) || ((make --no-print-directory -C $(BUILDSYS_PATH) $(PLATFORM) VERSION_$(ACTIVE_TARGET)=$(ACTIVE_VERSION) WRITE_OWNER_PATH=$(ACTIVE_OWNER_PATH) $(ACTIVE_TARGET) do $(BUILD_TYPE) 2>&1 | tee $@); echo $$? > $(OUTPUT_DIR)/$*.status);
ifneq ($(SPEC_LOG),)
	@printf "Output for $(P_STRING):\n" >> $(SPEC_LOG)
endif
	
	@if [[ "`cat $(OUTPUT_DIR)/$*.status 2> /dev/null || echo -1`" == "0" ]]; then\
		echo -e "$(PRINT_PFX)\t[ OK ] \t$(P_STRING) built without issues";\
		printf "Built without issues, status=0\n\n" >> $(SPEC_LOG);\
	else\
		echo -e "$(PRINT_PFX)\t[FAIL]\t$(P_STRING) failed with exit status `cat $(OUTPUT_DIR)/$*.status 2> /dev/null || echo -1`";\
		cat $@ >> $(SPEC_LOG);\
		printf "\n\n" >> $(SPEC_LOG);\
		(echo `cat $(OUTPUT_DIR)/$*.status 2> /dev/null || echo -1`) > $(OUTPUT_DIR)/$(SPEC).fail;\
		$(TARGET_FAIL_INVOKE) $(ACTIVE_TARGET) $(ACTIVE_VERSION) $(PLATFORM) `cat $(OUTPUT_DIR)/$*.status 2> /dev/null || echo -1` $@ "`cat $(ACTIVE_OWNER_PATH) 2> /dev/null || echo ''`" $(TARGET_FAIL_SUFFIX);\
	fi\
	

.PHONY: all $(OUTPUT_FILES) clean mkOutputDir

clean: mkOutputDir
	@rm -r $(OUTPUT_DIR)/* > /dev/null 2>&1 || true
