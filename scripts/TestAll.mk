MAKEFLAGS += --no-print-directory

BUILD_TYPE ?= debug
BUILDSYS_PATH = $(realpath $(dir $(shell pwd)/../))
SPEC_DIR = $(realpath $(BUILDSYS_PATH)/specs)
SCRIPT_DIR = $(realpath $(BUILDSYS_PATH)/scripts)
PLATFORM_DIR = $(realpath $(BUILDSYS_PATH)/platforms)
PLATFORMS ?=$(filter-out Platform,$(subst .mk,,$(notdir $(wildcard $(PLATFORM_DIR)/*.mk))))

SPEC_LIST ?=$(notdir $(wildcard $(SPEC_DIR)/*.mk) )
SPEC_LIST_NAMES = $(SPEC_LIST:%.mk=%)

#SPEC_FAIL_CMD ?= $(SCRIPT_DIR)/specFail.sh
#TARGET_FAIL_CMD ?= $(SCRIPT_DIR)/targetFail.sh

OUTPUT_DIR = $(SCRIPT_DIR)/.testAll
OUTPUT_SPECS = $(SPEC_LIST_NAMES:%=$(OUTPUT_DIR)/%.txt)
OUTPUT_FILES = $(foreach PLATFORM, $(PLATFORMS), $(OUTPUT_SPECS:%.txt=%__$(PLATFORM).txt))

TESTSPEC_DIR = $(SCRIPT_DIR)/.testSpec

default: all

prePrint:
	@echo "Platforms to be tested: $(PLATFORMS)"
	@echo "Specs to be tested: $(SPEC_LIST_NAMES)"
	@echo "Temporary output to: $(OUTPUT_DIR)"
	@echo "Updating Buildsys to newest version..."
	@(make --no-print-directory -C $(BUILDSYS_PATH) buildsysUpdate sysinstall)
	@echo ""
#@echo "Begin spec targets:"
	
mkOutputDir:
	@mkdir -p $(OUTPUT_DIR)
cleanOutputDir:
#@rm -r $(OUTPUT_DIR)/*

cleanSpec:
	@make -C $(SCRIPT_DIR) -f TestSpec.mk clean
	
all: prePrint mkOutputDir cleanOutputDir cleanSpec $(OUTPUT_FILES)

TARGET_FAIL_CMD_PASS ?=
ifdef TARGET_FAIL_CMD
	TARGET_FAIL_CMD_PASS = TARGET_FAIL_CMD=$(TARGET_FAIL_CMD)
endif

ifdef SPEC_FAIL_CMD
	SPEC_FAIL_INVOKE = $(SPEC_FAIL_CMD)
	SPEC_FAIL_SUFFIX =
else
	SPEC_FAIL_INVOKE = echo
	SPEC_FAIL_SUFFIX = >/dev/null 2>&1 || true
endif

ifdef SKIP_PLATFORM
	TARGET_FAIL_CMD_PASS += SKIP_PLATFORM=true
endif

	
# > $@ 2>&1;
#to split output: | tee $@
$(OUTPUT_FILES): $(OUTPUT_DIR)/%.txt:
	$(eval ACTIVE_PLATFORM := $(subst .mk,,$(lastword $(subst __, ,$*))))
	$(eval ACTIVE_SPEC := $(firstword $(subst __, ,$*)))
	$(eval ACTIVE_SPEC_PATH := $(SPEC_DIR)/$(ACTIVE_SPEC))
	
	@rm $@ 2>/dev/null || true
	@printf "Log for $(ACTIVE_SPEC):$(ACTIVE_PLATFORM) in $(ACTIVE_SPEC_PATH)\n\n" >> $@
	
#@echo -ne "\t$(ACTIVE_SPEC) for $(ACTIVE_PLATFORM) in progress, from $(ACTIVE_SPEC_PATH) to $* ...\r"
	@(make --no-print-directory -C $(SCRIPT_DIR) -f TestSpec.mk SPEC=$(ACTIVE_SPEC) PLATFORM=$(ACTIVE_PLATFORM) BUILD_TYPE='$(BUILD_TYPE)' SPEC_LOG='$@' $(TARGET_FAIL_CMD_PASS) 2>&1) ; echo `cat $(TESTSPEC_DIR)/$(ACTIVE_SPEC).fail 2>/dev/null || echo 0` > $(OUTPUT_DIR)/$*.status
#format for printing (as opposed to shell)
#@(cat $@ | sed 's/.*\r//') > $@
	
	@if [[ "`cat $(OUTPUT_DIR)/$*.status 2> /dev/null || echo -1`" == "0" ]]; then\
		echo -e "[ OK ]\t$(ACTIVE_SPEC) for $(ACTIVE_PLATFORM) built without issues\n";\
	else\
		echo -e "[FAIL]\t$* failed with exit status `cat $(OUTPUT_DIR)/$*.status 2> /dev/null || echo -1`, log stored in $@\n";\
		$(SPEC_FAIL_INVOKE) $(ACTIVE_SPEC) $(ACTIVE_PLATFORM) `cat $(OUTPUT_DIR)/$*.status 2> /dev/null || echo -1` $@ $(SPEC_FAIL_SUFFIX);\
	fi\

.PHONY: all cleanSpec mkOutputDir prePrint $(OUTPUT_FILES)
