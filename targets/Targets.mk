#Assign lowercase as raw name if it doesn't exist
T_RAW_NAME ?= $(shell echo $(T_NAME) | tr A-Z a-z)
TARGET_MAIN_POST ?=

SUBMAKE_PREFIX ?= 
BUILDSYS_AUTO=true
#SUBMAKE_PREFIX += BUILDSYS_AUTO=true 

#T_VERSION = $(VERSION_$(T_NAME))
#T_RAW_VERSION = $(VERSION_$(T_RAW_NAME))
#$(T_VERSION) ?= trunk
#ifeq ($(T_VERSION),)
#ifdef VERSION_$(T_RAW_NAME)
#T_VERSION = $(T_RAW_VERSION)
#endif
#endif
#ifeq ($(T_VERSION),)
#T_VERSION=trunk
#endif

#ifdef VCS_FORCE_REV
#T_VERSION_FOLDER_PREFIX=r$(VCS_FORCE_REV)_
#else
T_VERSION_FOLDER_PREFIX=
#endif

#ifeq ($(T_VERSION),NA)
T_VERSION_FOLDER=.
#else
#	T_VERSION_FOLDER=$(T_VERSION)
#endif
#T_VERSION_FOLDER=$(T_VERSION_FOLDER)
ifndef LPBUILD_GIT_NO_SSH
ifeq ($(findstring github,$(T_VCS_PATH)),github)
T_VCS_FULL=$(subst https://github.com/,git@github.com:,$(T_VCS_PATH))
else
T_VCS_FULL=$(subst https://bitbucket.org/,git@bitbucket.org:,$(T_VCS_PATH))
endif
else
T_VCS_FULL ?= $(T_VCS_PATH)/$(T_VERSION_FOLDER)
endif

T_LCL_PATH_MSG = 
T_VARNAME_MEXP = MEXP_PATH_$(T_NAME)_$(T_VERSION)

ifdef $(T_VARNAME_MEXP)
	T_DEST = $($(T_VARNAME_MEXP))
	T_LCL_PATH_MSG = "Running '$(strip $(CHILD_ACT_TARGETS))' in active repository for $(T_NAME) $(T_VERSION): $(T_DEST)"
else
ifndef AUTO_REPO
	T_DEST =$(subst END,,$(subst /.END,,$(BUILDS_DIR)/$(T_DIR_NAME)/$(T_VERSION_FOLDER_PREFIX)$(T_VERSION_FOLDER)END))
define MK_VCS_PROMPT =
(test -d $(T_DEST) && (
read -p "Use existing project folder at $(T_DEST) [y/n]?: " choice;
case "$$choice" in 
  y|Y ) echo "$$(cp .bsid $(T_DEST)/.bsid; cd $(T_DEST)/ && make metadata; echo "0")";; 
  n|N ) echo "1";; 
  * ) echo "1";; 
esac;true)) || ((test -d $(T_DEST) && echo 0) || (
read -p "Clone new repository to $(T_DEST) [y/n]?: " choice;
case "$$choice" in 
  y|Y ) echo "0";; 
  n|N ) echo "1";; 
  * ) echo "1";; 
esac;true)) || echo 1
endef
#$(info ${MK_VCS_PROMPT})
ifeq ($(shell ${MK_VCS_PROMPT}),1)
$(error No active repository for $(T_NAME))
endif
else
T_DEST :=$(BUILDS_DIR)/$(T_DIR_NAME)/$(T_VERSION_FOLDER_PREFIX)$(T_VERSION_FOLDER)
AUTO_SET:=$(shell cp .bsid $(T_DEST)/.bsid; cd $(T_DEST)/ && make metadata)
T_LCL_PATH_MSG :="Running '$(strip $(CHILD_ACT_TARGETS))' in local respository for  $(T_NAME) $(T_VERSION): $(T_DEST)"
endif
#T_LCL_PATH_MSG = "Running '$(strip $(CHILD_ACT_TARGETS))' in local respository for  $(T_NAME) $(T_VERSION): $(T_DEST)"
endif

VCS_META_FILE = .git
VCS_META_PATH = $(T_DEST)/$(VCS_META_FILE)

CHILD_SPEC_TARGETS = $(MEXP_TARG_$(T_NAME)_$(T_VERSION)) $(SPEC_TARG_$(T_NAME)_$(T_VERSION)) $(SPEC_TARG_$(T_NAME)) $(SPEC_TARG_CLS_$(T_CLASS))
CHILD_ACT_TARGETS = $(CHILD_TARGETS) $(CHILD_SPEC_TARGETS)

VCS_ARGS ?= 
VCS_CM_ARGS :=$(VCS_ARGS)

$(VCS_META_PATH): vcs_checkout
vcs_checkout:
	@+if [ -d "$(T_DEST)" ]; then :; else \
	git clone $(VCS_ARGS) "$(T_VCS_FULL)" "$(T_DEST)"; \
	fi;\
	cp .bsid $(T_DEST)/;\
	cd $(T_DEST)/; make metadata; true 

#vcs_update: $(T_DEST) local_bsid_tag

#vcs_commit: $(T_DEST)

#vcs_delete: $(T_DEST)
	

local_source: $(VCS_META_PATH)
	@echo "$(T_LCL_PATH_MSG)"
	
local_bsid_tag: 
	@(rm $(T_DEST)/.bsid || true) 2> /dev/null
	@cp .bsid $(T_DEST)/

T_EX_REQS ?=
ifdef WRITE_OWNER_PATH
T_EX_REQS += writeOwners
endif
ifndef VCS_FORCE_EXPORT
T_EX_REQS += $(VCS_INFO_FILE)
endif
ifdef VCS_DO_COMMIT
T_EX_REQS += vcs_commit
endif
	
$(T_DEST): local_source local_bsid_tag $(T_EX_REQS)
	
ex_deps:
	
	
writeOwners:
ifdef WRITE_OWNER_PATH
	@(echo "$(T_OWNERS)" | xargs echo -n) > $(WRITE_OWNER_PATH) || true
endif

JOBS := $(shell echo "$$((`nproc --all` + `nproc --all`/2))") 
MK_ARGS += -j$(JOBS) BUILDSYS_AUTO=true JOBS=$(JOBS)

ifdef COMMIT_MSG
MK_ARGS += COMMIT_MSG="$(COMMIT_MSG)"
endif
ifdef VERBOSE
MK_ARGS += VERBOSE=$(VERBOSE)
endif
#SUBMAKE_PREFIX += JOBS=$(JOBS)

