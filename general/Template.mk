default:debug

BUILD_DIR ?= build

ENFORCE_PCH?=true

ifeq ($(findstring verbose,$(MAKECMDGOALS)),verbose)
VERBOSE :=true
endif

ifndef TARGET_PLATFORM
ifdef PLATFORM
TARGET_PLATFORM = $(PLATFORM)
endif
endif

TARGET_PLATFORM ?= Desktop
ifeq ($(TARGET_PLATFORM),)
OBJ_DIR ?= $(BUILD_DIR)
else
OBJ_DIR ?= $(BUILD_DIR)/$(TARGET_PLATFORM)
endif
OUT_DIR = $(OBJ_DIR)

mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
ABS_DIR := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))
CUR_DIR ?= $(CURDIR)

TEMPLATED_USED:=true
-include /usr/include/LPB.mk
TARG_DIR ?= usr/bin

CC_SYS_FLAGS ?=
SPEC_SRC_DIRS ?=specs
SPEC_EXTRA_DIR ?=

#SRC_DIRS = lib classes
#HEADER_DIRS = lib
HEADERS_OUT_DIR ?= $(SYS_NAME)/

PCH_OUT_DIR :=$(OUT_DIR)/.pch
TMP_OUT_DIR :=$(OUT_DIR)/.tmp

HEADER_FILES += $(HEADER_GLOBAL_FILES)

CT_DIR :=$(BUILD_DIR)/CTData
CT_DATA :=$(strip $(foreach df, $(CT_DATA_FILES),$(eval CT_DATA_PATH_$(shell echo -n "$(df)" | sed "s/[\/.]/_/g"):=$(df))\
$(CT_DIR)/$(dir $(df))$(basename $(notdir $(df))).cpp))

CT_OBJS :=$(CT_DATA:%.cpp=%.o)
#$(info D=$(CT_DATA))
#$(info F=$(CT_DATA_FILES), OBJS=$(CT_OBJS))

PCH ?=
OBJS += $(foreach srcDir, $(SRC_DIRS), $(patsubst $(srcDir)%.cpp,$(OBJ_DIR)/$(srcDir)%.o,$(shell find $(srcDir)/ $(FIND_EX_$(srcDir)) -type f -name '*.cpp' 2>/dev/null)))
EXECS +=$(foreach srcDir, $(EXEC_DIRS), $(patsubst $(srcDir)%.cpp,$(OUT_DIR)/%,$(wildcard $(srcDir)*.cpp)) )
HEADER_FILES += $(sort $(foreach srcDir, $(HEADER_DIRS), $(shell find $(srcDir)/ -type f \( -name "*.h" -o -name "*.hpp" \) 2>/dev/null)))

DYNS +=$(strip $(foreach srcDir, $(SPEC_SRC_DIRS), $(patsubst $(srcDir)/%.dyn.txt,$(OUT_DIR)/lib%.so,$(wildcard $(srcDir)/*.dyn.txt)) ))
STATICS +=$(strip $(foreach srcDir, $(SPEC_SRC_DIRS), $(patsubst $(srcDir)/%.stc.txt,$(OUT_DIR)/lib%.a,$(wildcard $(srcDir)/*.stc.txt)) ))
EXECS +=$(strip $(foreach srcDir, $(SPEC_SRC_DIRS), $(patsubst $(srcDir)/%.exe.txt,$(OUT_DIR)/%,$(wildcard $(srcDir)/*.exe.txt)) ))

#$(info Dyns=$(DYNS), Statics=$(STATICS), Execs=$(EXECS))

EXECS := $(sort $(EXECS))

OUT_HNAMES := $(EXECS:$(OUT_DIR)/%=%) $(STATICS:$(OUT_DIR)/%=%) $(DYNS:$(OUT_DIR)/%=%)

ORIG_FS = $(FS)
ifeq ($(FS),)
FS = /
#@echo "FS env variable is not defined, defaulting to '/'!"
endif
ifeq ($(FSDEV),)
FSDEV = /
#@echo "FSDEV env variable is not defined, defaulting to '/'!"
endif

SHELL := /bin/bash

#point to slash on non-device builds
debug release install:: FS ?= /
debug release install:: FSDEV ?= /

CXX ?= g++
CC ?= gcc
OLDCXX ?= g++
OPTI ?= -O0
debug:: CXXPLUS += $(OPTI) -g -DDEBUG
release:: CXXPLUS += $(OPTI)  -DOPTIMIZE -DNDEBUG

SUB_MAKE_ARGS:= CXX=$(CXX) CC=$(CC) FS=$(FS) FSDEV=$(FSDEV) QMAKE=$(QMAKE) CFLAGS="$(OPTI)" LDFLAGS="" LFLAGS="" 

LIBS ?=
LIB_PATHS ?=
EXEC_LIBS ?=

LIBS := $(foreach lib, $(LIB_PATHS), -L$(shell realpath $(lib)) -Wl,-rpath,$(shell realpath $(lib))) $(LIBS)
WARNINGS ?= -Wall -Wextra -Wnon-virtual-dtor -Wcast-align -Wshadow -Wno-unused-result -Wno-strict-aliasing

CXXPLUS += -march=native -MMD -MP -fdollars-in-identifiers -fnon-call-exceptions -fconcepts

ifneq ($(strip $(PCH)),)
ifeq ($(ENFORCE_PCH),true)
CXXPLUS += -I$(PCH_OUT_DIR) -include $(PCH) -I$(TMP_OUT_DIR)
else
CXXPLUS += -I$(PCH_OUT_DIR) -include $(PCH)
endif
endif

CXXPLUS += -I$(CUR_DIR) -I$(FSDEV)/usr/include/
CFLAGS += -std=gnu++2a -fms-extensions -fconcepts $(CXX_SYS_FLAGS) -Wl,--no-as-needed -pipe -c $(CXXPLUS)
BFLAGS += -pipe -c $(CXXPLUS)
CFLAGSNW += $(CFLAGS)
CFLAGS += $(WARNINGS)
LFLAGS += -std=gnu++2a -fms-extensions -fconcepts $(CXX_SYS_FLAGS) $(WARNINGS) -pipe -I./ $(CXXPLUS) -L$(OBJ_DIR) -L$(FS)/usr/lib -L$(FSDEV)/usr/lib 
#LFLAGS += -Wl,-rpath,{{}}

INCPATH ?= 
HEADER_INCS := $(foreach srcDir, $(HEADER_DIRS),$(addprefix -I,$(srcDir)))
INCPATH += $(HEADER_INCS)

devInstall device:: INCPATH += -I$(FSDEV)usr/include/
debug::   LFLAGS +=
release:: LFLAGS +=
#dyn::	
CFLAGS += -shared -fPIC

CFLAGS += $(INCPATH)

$(OUT_DIR): .lpbmeta
	@mkdir -p $(OUT_DIR)
	
HEADERS_OUT_DIR:=$(strip $(HEADERS_OUT_DIR))

ALL_SRCS:=$(HEADER_FILES) $(CT_DATA_FILES)
ALL_SRCS+=Makefile .gitignore $(wildcard *.kdev*) $(wildcard .kdev4/*)

.SECONDEXPANSION:
PCH_OUT :=$(strip $(foreach header,$(PCH),\
$(strip $(eval stem:=$(basename $(header)))\
$(eval PCH_PATH_$(stem):=$(header))\
$(PCH_OUT_DIR)/$(stem:%=%.h.gch))\
))
RELATIVE_OUT_DIR = --relative-to=./

#because make is incapable of parenthesis
FLMC_1 = find . -not -path '*/\.*' -type f \( 
FLMC_2 =  \) -exec echo {} + 2>/dev/null | tr " " "\n" | grep -E "cpp|h" | tr "\n" " "
#SRC_DIRS restriction: no longer needed; | grep -E "$$(echo '$(SRC_DIRS)'| sed 's/ /|/g')"

FIND_ARGS := -not -path '*/\.*' -type f -path "
FIND_POST :=" -printf '%P\n' | sed "s/^\.\///g" | grep -vE "*\.o|*\.d|*\.tmp" | sed "s/^.\///g" | tr "\n" " "
#$(eval BPATS := $(shell (cat $(SPEC_SRC_DIRS)/$(CUR_TARG).$2.txt;echo) | sed "s/^/\ -o \-wholename \'.\//" | tr "\n" "\' \-o " | sed "s/^\ -o //"))
#$(eval BFILES := $(shell $(FLMC_1)$(BPATS) $(FLMC_2)))
#$(info $(eval BFILES := $(foreach patt,$(BPATTS),$(shell find $(dir $(patt)) $(FIND_ARGS) $(notdir $(patt)) $(FIND_POST)))))

HASHMARK = $(shell echo "\#")
PERCENT := %

#auto-gen [deprecated]
#$(eval $(shell stat $(SPEC_FILE) > /dev/null 2>&1 || (mkdir -p $(SPEC_SRC_DIRS) && printf "\n" > $(SPEC_FILE)) ))
#
#$(eval $(info FOUND))
#$(eval $(info ATTEMPT=$(CUR_TARG), resolves=$(strip $(firstword $(wildcard $(SPEC_SRC_DIRS)/$(CUR_TARG).$2.txt) $(wildcard $(SPEC_EXTRA_DIR)/$(CUR_TARG).$2.txt)))))
#$(eval $(info canidates=$(SPEC_SRC_DIRS)/$(value CUR_TARG).$2.txt  $(SPEC_EXTRA_DIR)/$(value CUR_TARG).$2.txt))

define dynReqs =
	$(if $(strip $1 $2),$(eval
	$(eval TMP_TARG:=$1)
	$(eval LCL_DYNS:=)
	$(eval CUR_TARG:=$(subst xxx,,$(subst xxxlib,xxx,xxx$(basename $(TMP_TARG:$(OUT_DIR)/%=%)))))
	
	$(if $(strip $(firstword $(wildcard $(SPEC_SRC_DIRS)/$(CUR_TARG).$2.txt) $(wildcard $(SPEC_EXTRA_DIR)/$(CUR_TARG).$2.txt))),$(eval
	
	$(eval SPEC_FILE:=$(firstword $(wildcard $(SPEC_SRC_DIRS)/$(CUR_TARG).$2.txt) $(wildcard $(SPEC_EXTRA_DIR)/$(CUR_TARG).$2.txt)))
	$(eval BPATS := $(strip $(shell (cat $(SPEC_FILE);echo) | grep -vE "^\&|^\+|^\$(HASHMARK)|^\!|^>" | tr "\n" " ")) )
	$(eval BFILES := $(foreach patt,$(BPATS),$(addprefix $(subst @@@,,$(strip $(dir $(patt)))),$(shell find $(strip $(dir $(patt))) $(FIND_ARGS)$(dir $(patt))$(strip $(notdir $(patt)))$(FIND_POST)))))
	$(eval BIMPL := $(foreach f,$(BFILES),$(if $(findstring .cpp!,$f!),$f,)) )
	$(eval BHEAD := $(foreach f,$(BFILES),$(if $(findstring .h!,$f!),$f,)) )
	$(eval EX_LIB_$(strip $2)_$(CUR_TARG) += $(addprefix -l,$(strip $(foreach deplib,$(shell \
		(cat $(SPEC_FILE);echo) | grep "^+" | cut -c2- | tr "\n" " "),\
 		$(eval LCL_DYNS+= $(addprefix $(OUT_DIR)/,$(eval TMPLL:=$(notdir $(firstword $(strip $(wildcard $(SPEC_EXTRA_DIR)/$(deplib).dyn.txt)  ))))$(TMPLL:%.dyn.txt=lib%.so))\
		) $(firstword $(strip $(TMPLL:%.dyn.txt=%) $(deplib)))))))
	$(eval EX_SUBMAKES_RAW:=$(shell (cat $(SPEC_FILE);echo) | grep "^!" | tr "^!" " " | tr ":" " " | tr "\-\>" " " | sed 's/  \+/ /g' | tr " " ":" | cut -c 2-
	))
	$(eval EX_CMDS_$(strip $2)_$(CUR_TARG)+=$(strip $(foreach sm,$(strip $(EX_SUBMAKES_RAW)),$(strip \
		$(eval sm_dir:=$(dir $(shell echo "$(sm)" | awk -F: '{print $$1}')))\
		$(eval sm_mk:=$(notdir $(shell echo "$(sm)" | awk -F: '{print $$1}')))\
		$(eval sm_file:=$(strip $(shell echo "$(sm)" | awk -F: '{print $$2}')))\
		$(eval sm_out:=$(notdir $(shell echo "$(sm)" | awk -F: '{print $$3}')))\
		sudo -iu $(USER) bash -c "cd $(shell realpath $(sm_dir)) && $(SUB_MAKE_ARGS) make -f $(sm_mk) > /dev/null" > /dev/null && \
		cp $(shell realpath $(sm_dir)/$(sm_file)) $(shell realpath $(OUT_DIR)/$(sm_out))); \
	)) )
	$(eval EX_CMDS_$(strip $2)_$(CUR_TARG) += $(foreach cmd,$(strip $(shell (cat $(SPEC_FILE);echo) | grep "^>" | cut -c2- | xargs -n1 echo )), $(cmd)))
	$(eval EX_NODEP_LIB_$(strip $2)_$(CUR_TARG) += $(addprefix -l,$(foreach lb,$(strip $(shell (cat $(SPEC_FILE);echo) | grep "^$(PERCENT)" | cut -c2- | xargs -n1 echo )), $(lb))))
	$(eval EX_DATA_$(strip $2)_$(CUR_TARG) += $(foreach datap, $(strip $(shell (cat $(SPEC_FILE);echo) | grep "^&" | cut -c2- | tr "\n" " ")),$(filter-out %*, $(datap))  $(addprefix $(dir $(datap)), $(shell find $(dir $(datap)) $(FIND_ARGS)$(dir $(datap))$(notdir $(datap))$(FIND_POST)))))
	$(eval ALL_SRCS+=$(sort $(BHEAD) $(wildcard $(BIMPL:%.cpp=%.h))) $(BIMPL) $(SPEC_FILE) $(EX_DATA_$(strip $2)_$(CUR_TARG)))
	$(eval BOBJS :=$(BIMPL:%.cpp=$(OUT_DIR)/%.o))
	$(eval DYNS+=$(LCL_DYNS))
	$(eval BOBJS_$(strip $2)_$(CUR_TARG) :=$(BOBJS))
	$(eval BDYNS_$(strip $2)_$(CUR_TARG) :=$(LCL_DYNS) $(EX_NODEP_LIB_$(strip $2)_$(CUR_TARG)))
	$(eval BOPTIONALS_$(strip $2)_$(CUR_TARG) :=$(EX_NODEP_LIB_$(strip $2)_$(CUR_TARG)))
	
	))   ),)
endef
#pushd $(sm_dir) && $(SUB_MAKE_ARGS) make -f $(sm_mk) && popd &&\

#$(info For $2 $(CUR_TARG), BOBJS_$(CUR_TARG)=$(BOBJS))
#$(info PATS=$(BPATS))
#$(info FILES=$(BFILES))
#$(info CMD=$(FLMC_1)$(BPATS) $(FLMC_2))
#$(info DAT=$(EX_DATA_$(strip $2)_$(CUR_TARG)))
#$(eval $(info IMPL=$(BIMPL:%.cpp=$(OUT_DIR)/%.o)))
#$(eval $(info LCLDYNS=$(LCL_DYNS)))
#$(eval $(info OBJS var = BOBJS_$(strip $2)_$(CUR_TARG) === $(BOBJS_$(strip $2)_$(CUR_TARG))))

#$(info SRCS=$(ALL_SRCS))

define ignore =
	$(shell (printf "$(subst $(SPACE),$(NEWLINE),$(strip $1))" >> .gitignore) && (echo "`(sort .gitignore | sed 's/ *//' | sed '/^$$/d' | uniq)`" > .gitignore))
endef
define addCTData =
	$(if $(strip $1),$(eval
	$(eval VNAME:=_ct_$(shell echo -n "$1" | sed "s/[\/.]/_/g"))
	$(eval CTF_FILE_$(shell echo -n "$(basename $1)" | sed "s/[\/.]/_/g"):=$1)
	$(eval CTCPP:=$(basename $(CT_DIR)/$1).cpp)
	$(eval CTPTSTR:=$(basename $(CT_DIR)/$1).o: $1 )
ifeq ($(wildcard $(CT_DIR)/CTData.h),)
	$(shell mkdir -p $(CT_DIR); touch $(CT_DIR)/CTData.h; printf "#include <string>\nextern std::unordered_map<std::string,const std::string*>& _CTMap;\n")
endif
	$(shell printf "std::string $(VNAME);\n" >> $(CT_DIR)/CTData.h && (echo "`(cat $(CT_DIR)/CTData.h | sed '/^$$/d' | uniq)`" > $(CT_DIR)/CTData.h))
	$(shell mkdir -p $(CT_DIR)/$(dir $1))
	$(shell (printf "$(CTPTSTR)") > $(basename $(CTCPP)).mk)
	$(shell mkdir -p $(dir $(CTCPP));
	printf '#include <string>\n#include <unordered_map>\nstd::string $(VNAME) = R"H4X0R4U(' > $(CTCPP);
	dd if=$1 bs=1 status=none >> $(CTCPP) 2> /dev/null;
	printf ")H4X0R4U\";\nextern std::unordered_map<std::string,const std::string*>& _CTMap;\nstruct CTINS_$(VNAME) {CTINS_$(VNAME)() {_CTMap[\"$1\"] = &$(VNAME);}}; struct CTST_$(VNAME) {static CTINS_$(VNAME) __attribute__((used)) ins;};\n
	" >> $(CTCPP))
	),)
endef
#TBF:=$(info Before loop- execs=$(EXECS))
PSE := $(foreach obj,$(EXECS),$(eval $(call dynReqs,$(obj),exe))) \
$(foreach obj,$(DYNS),$(eval $(call dynReqs,$(obj),dyn))) \
$(foreach obj,$(STATICS),$(eval $(call dynReqs,$(obj),stc))) \
$(foreach df,$(CT_DATA_FILES), $(eval $(call addCTData,$(df)))\
$(eval DYNS:=$(sort $(DYNS)))\
$(eval STATICS:=$(sort $(STATICS)))\
$(eval EXECS:=$(sort $(EXECS)))\
)
#TBE:=$(info After loop)
pre :: 
debug :: pre $(STATICS) $(DYNS) $(EXECS) 
release :: pre $(STATICS) $(DYNS) $(EXECS) 
dyn :: $(DYNS) 
static :: $(STATICS) 
exec :: $(EXECS) 
build::debug 
verbose:: 

# pull in dependency info for *existing* .o files
#-include $(OBJS:.o=.d)
#SRC_DEP_FILES = $(shell find . -type f -name '*.d' -exec echo {} + 2>/dev/null | tr " " "\n" | grep -vE "$$(echo '$(EXECS:%=%.d)' | sed 's/ /|/g')" | tr "\n" " ")
SRC_DEP_FILES = $(strip $(shell find $(OUT_DIR) -type f -name '*.d' -exec echo {} + 2>/dev/null) \
$(shell find $(CT_DIR) -type f -name '*.d' -exec echo {} + 2>/dev/null) \
$(shell find . -maxdepth=1 -type f -name '*.d' -exec echo {} + 2>/dev/null))
#$(info Including $(SRC_DEP_FILES))

-include $(SRC_DEP_FILES)
#cmdst=$$?; rcmd=$$(fc -ln | tail -n 1 | head -n 1); test -z $$cmdst
#echo "CMD=$$rcmd";
# | sed -r 's/\\(.)/\1/g'
LCLR:=\e[K
ifdef VERBOSE
VERBOSE_EX:=; printf "\t\t$$rcmd\n"
endif

#removed indentation so editors wouldn't complain
#| sed 's/^/\t\t/'
#(printf "\n\t\t[CMD] Failed command was:\n\t\t"; echo "$$rcmd");
# printf "\n\t\t[FROM CMD] $$rcmd \n"

CXX_PRE := printf "\t[...] $$RFN in progress...\r"; export rcmd=$$(printf %q "
CXX_POST := " | ( read bat; echo $$bat; ) ); \
			cmdret=0;\
			cmdout=$$( set -o pipefail && \
			($$rcmd 2>&1  |  xargs -d'\n' -I {} echo "{}") && \
				(($$? == 0)) \
			) || ( \
		printf "$(LCLR)\t[ERR] $$RFN\n";\
		printf "$$rcmd\n$$cmdout";\
		false\
	) && \
	(\
		(test -z "`xargs <<< $$cmdout`" && printf "$(LCLR)\t[OK] $$RFN\n"$(VERBOSE_EX)) ||\
		(printf "$(LCLR)\t[WARN] $$RFN\n"; printf "$$rcmd\n$$cmdout";true)\
	);
#echo "Command got \"$$cmdout\" from $$RFN"

$(OUT_HNAMES):%: $(OUT_DIR)/% 
	@

$(PCH_OUT_DIR): 
	@mkdir -p $(PCH_OUT_DIR)
$(TMP_OUT_DIR): 
	@mkdir -p $(TMP_OUT_DIR)
	
-include $(wildcard $(PCH_OUT_DIR)/$(PCH).d)

$(PCH_OUT): $(PCH_OUT_DIR)/%.h.gch: $$(value PCH_PATH_%) $(wildcard $(PCH_OUT_DIR)/%.h.d) $(CT_OBJS) | $(PCH_OUT_DIR) $(TMP_OUT_DIR)
	$(eval HTMP:=$(TMP_OUT_DIR)/$*.h)
	$(eval header:=$(value PCH_PATH_$*))
	$(eval ALL_SRCS +=$(shell echo "$+" | tr " " "\n" | grep -E ".h|.cpp" | tr "\n" " "))
#@echo "PCH for $(header) -> $@, tmp=$(HTMP)"
	@echo "#error Mandatory PCH for $(header) could not be included, bailing out..." > $(HTMP)
#gcc -c $(CPPFLAGS) -o $@ $(header)
#@ RFN="$<"; $(CXX_PRE) $(CXX) $(CFLAGS) $< -o $@ $(CXX_POST)
#@rm $(PCH_OUT_DIR)/$*.h 2>/dev/null || true
#@ln -s $(shell realpath $(header)) $(PCH_OUT_DIR)/$*.h
	@RFN="[PCH] $(header) -> $@"; $(CXX_PRE) $(CXX) -Winvalid-pch -x c++-header $(CFLAGS) -Ofast $(header) -o $@ $(CXX_POST)

#dependent builds
.cpp.o:: %.cpp $(PCH_OUT) $(wildcard %.h) 
	@mkdir -p "$(@D)"
#$(eval ALL_SRCS +=$(shell echo "$+" | tr " " "\n" | grep -E ".h|.cpp" | tr "\n" " "))
	@ RFN="$<"; $(CXX_PRE) $(CXX) $(CFLAGS) $< -o $@ $(CXX_POST)

#No longer needed, thanks to autodeps
#%.cpp $(wildcard %.h)
$(CT_OBJS): 
	$(eval CTFBASE:=$(basename $@))
#$(info BASE=$(CTFBASE) gives CTF_FILE_$(CTFBASE:$(CT_DIR)/%=%) for $(value CTF_FILE_$$(shell echo -n "$(CTFBASE:$(CT_DIR/%=%))" | sed "s/[\/.]/_/g")))
#$(eval CTFN:=$(value CTF_FILE_$(shell echo -n "$(CTFBASE:$(CT_DIR/%=%))" | sed "s/[\/.]/_/g")))
	$(eval CTFN:=$(wildcard $(CTFBASE:$(CT_DIR)/%=%).*))
	@mkdir -p "$(@D)"
	@RFN="[CTDATA] $(CTFN) -> $(CTFBASE)"; $(CXX_PRE) $(CXX) $(CFLAGS) $(PIC) $(CTFBASE).cpp -o $@ $(CXX_POST) 
	@rm $(CTFBASE).d 2>/dev/null || true

$(OUT_DIR)/%.o:: %.cpp $(PCH_OUT) 
	@mkdir -p "$(@D)"
	@RFN="$<"; $(CXX_PRE) $(CXX) $(CFLAGS) $(PIC) $< -o $@ $(CXX_POST) 

HIDDEN_FILES=$(filter-out .gitignore,$(filter-out .lpbmeta,$(notdir $(shell find . -maxdepth 1 -type f -name ".*" | tr "\n" " ")) ))
	
.lpbmeta:: $(filter-out, .kdev4, $(filter-out, $(OUT_DIR),$(shell find . -maxdepth 1 -type d -name "*" | grep -oP "^./\K.*" | tr "\n" " "))) $(HIDDEN_FILES)
	$(call ignore,nbproject)
	$(call ignore,$(BUILD_DIR))
	$(foreach hidden,$(filter-out .gitignore,$(filter .%,$+)),$(call ignore,$(hidden)))
	$(call ignore,vgcore*)
	$(call ignore,callgrind*)
	$(call ignore,~*)
	$(call ignore,*.d)
	$(call ignore,*.gch)
	$(call ignore,*.out*)
	$(call ignore,.lpbmeta)
	$(call ignore,out.txt)
	$(call ignore,make.txt)
	$(call ignore,Thumbs.db)
	@touch ".lpbmeta"


$(EXECS): $(OUT_DIR)/%: $$(sort $(CT_OBJS) $(PCH_OUT) $$(value BOBJS_exe_%) $(wildcard $(EXEC_DIRS)/%.cpp) $$(value BDYNS_exe_%) $$(value EX_DATA_exe_%) ) | $(DYNS) $(STATICS)
#begin actual rule
	$(eval FN := $(notdir $*))
	$(eval ALL_SRCS +=$(shell echo "$+" | tr " " "\n" | grep -E ".h|.cpp" | tr "\n" " "))
#$(eval CUR_TARG := $(notdir $*))
	@printf "\t+ Building Executable: $(FN) -> $@\n"
#@echo "Depends on: $+"
	@echo "$(strip $(BOBJS_exe_$(FN)))" | tr ' ' '\n' | awk '{print "\t\t" $$0}'
#@printf "\t"
	@mkdir -p $(OUT_DIR)
	@rm $(OUT_DIR)/$(FN) 2> /dev/null || true
	@rm $(FN) 2> /dev/null || true
	@test -z "$(strip $(wildcard $(EXEC_DIRS)/$(FN).cpp) $(BOBJS_exe_$(FN)))" ||(\
		RFN="$(FN).cpp"; $(CXX_PRE) $(CXX) $(INCPATH) -L./ -L$(OUT_DIR)/ -Wl,-rpath,$(OUT_DIR) $(LFLAGS) -o $@ -DPROGRAM_NAME='$(FN)' -Wl,--start-group $(wildcard $(EXEC_DIRS)/$(FN).cpp) $(BOBJS_exe_$(FN)) $(LIBS) -Wl,--end-group -Wl,--start-group  $(LIBS) $(EX_LIB_dyn_$(FN)) $(EX_NODEP_LIB_dyn_$(FN)) $(EXEC_LIBS) $(EX_LIB_exe_$(FN)) $(EX_NODEP_LIB_exe_$(FN)) -Wl,--end-group -pthread $(EX_LIB_stc_$(FN)) $(CXX_POST)\
	)
	
	@$(EX_CMDS_exe_$(FN))
	
	@ln -s $(shell readlink -f $(OUT_DIR)/$(FN)) -r $(FN)
	$(call ignore, $(FN))


$(DYNS):: $(OUT_DIR)/lib%.so: $$(sort $(CT_OBJS) $(PCH_OUT) $(wildcard $(SPEC_SRC_DIRS)/%.dyn.txt) $(wildcard $(SPEC_EXTRA_DIR)/%.dyn.txt) $$(value BOBJS_dyn_%) $$(value BDYNS_dyn_%) $$(value EX_DATA_dyn_%) ) | $$(value BOPTIONALS_dyn_%)
	$(eval FN := $(notdir $*))
	@printf "\t+ Building Dynamic Library: $* -> $@\n"
	@echo "$(BOBJS_dyn_$(FN))" | tr ' ' '\n' | awk '{print "\t\t" $$0}'
	@ RFN="$(FN) from $(wildcard $(SPEC_SRC_DIRS)/$*.dyn.txt) $(wildcard $(SPEC_EXTRA_DIR)/$*.dyn.txt)"; $(CXX_PRE) $(CXX) -Wall -shared -fPIC $(LFLAGS) -o $@ -pthread -Wl,--start-group -Wl,-Bdynamic $(BOBJS_dyn_$(FN)) -Wl,--end-group -Wl,--start-group -Wl,-Bdynamic -Wl,--unresolved-symbols=ignore-all $(LIBS) $(EX_LIB_dyn_$(FN)) $(EX_NODEP_LIB_dyn_$(FN)) -Wl,--end-group $(EX_LIB_stc_$(FN)) $(EX_NODEP_LIB_stc_$(FN)) $(CXX_POST)
	@rm $(notdir $@) 2>/dev/null || true
	@ln -s $(shell readlink -f $@) -r $(notdir $@)
	$(call ignore, $(notdir $@))

$(STATICS):: $(OUT_DIR)/lib%.a: $$(sort $(CT_OBJS) $(PCH_OUT) $(SPEC_SRC_DIRS)/%.stc.txt $$(value BOBJS_stc_%) $$(value EX_DATA_stc_%) ) | $$(value BOPTIONALS_stc_%)
	$(eval FN := $(notdir $*))
	@printf "\t+ Building Static Library: $* -> $@\n"
	@echo "$(BOBJS_stc_$(FN))" | tr ' ' '\n' | awk '{print "\t\t" $$0}'
	@ar rs $@ $(BOBJS_stc_$(FN)) > /dev/null 2>/dev/null
	@rm $(notdir $@) 2>/dev/null || true
	@ln -s $(shell readlink -f $@) -r $(notdir $@)
	$(call ignore, $(notdir $@))

#secondary instructions
clean::  | $(OUT_DIR)
	@find $(OBJ_DIR) -name "*.o" -type f -delete 2>/dev/null || true
	@find $(OUT_DIR) -name "*.so" -type f -delete 2>/dev/null  || true
	@find $(OUT_DIR) -name "*.a" -type f -delete 2>/dev/null  || true
	@find $(OUT_DIR) -name "*.d" -type f -delete 2>/dev/null  || true
	@find $(OUT_DIR) -name "*.i" -type f -delete 2>/dev/null  || true
	@find ./ -maxdepth 1 -name "*.d" -type l -delete 2>/dev/null  || true
	@find ./ -maxdepth 1 -name "*.a" -type l -delete 2>/dev/null  || true
	@find ./ -maxdepth 1 -name "*.so" -type l -delete 2>/dev/null  || true
	@find ./ -maxdepth 1 -name "vgcore*" -type f -delete 2>/dev/null  || true
#@echo "WOULD HAVE DELETED $(EXECS)"
	-@rm $(EXECS) 2>/dev/null   || true
	-@rm $(EXECS:$(OUT_DIR)/%=%) 2>/dev/null   || true
#-@rm $(OUT_DIR)/$(EXECS) 2>/dev/null   || true
	-@rm -r $(OUT_DIR)/.pch 2>/dev/null   || true
	-@rm -r $(OUT_DIR)/.tmp 2>/dev/null   || true
	@rm .fuse_hidden* > /dev/null 2>&1 || true
	-@rm -r $(BUILD_DIR)/CTData 2>/dev/null   || true

install:: $(EXECS) $(DYNS) $(STATICS) 
	@echo "Install to $(FS), targeting $(FS)/$(TARG_DIR)"
ifneq ($(strip $(EXECS)),)
#@$(INS_PFX) cp $(EXECS) $(FS)/$(TARG_DIR)/
endif
ifneq ($(strip $(DYNS)),)
	@$(INS_PFX) cp $(DYNS) $(FS)/usr/lib/
#@$(INS_PFX) cp $(DYNS) $(FSDEV)/usr/lib/
endif
ifneq ($(strip $(STATICS)),)
	@$(INS_PFX) cp $(STATICS) $(FSDEV)/usr/lib/
endif
ifneq ($(strip $(HEADER_FILES)$(HEADER_CP_DIRS)),)
	@$(INS_PFX) mkdir -p $(FSDEV)/usr/include/$(HEADERS_OUT_DIR)/ $(addprefix $(FSDEV)/usr/include/$(HEADERS_OUT_DIR)/,$(HEADER_CP_DIRS))
	@$(INS_PFX) cp $(HEADER_FILES) $(FSDEV)/usr/include/$(HEADERS_OUT_DIR)/ || true
endif
ifneq ($(strip $(HEADER_CP_DIRS)),)
	@$(INS_PFX) cp -r $(HEADER_CP_DIRS) $(FSDEV)/usr/include/$(HEADERS_OUT_DIR)/ || true
	@$(INS_PFX) bash -c "$(foreach hgf,$(HEADER_GLOBAL_FILES),$(foreach hod,$(HEADER_CP_DIRS), ln -s $(FSDEV)/usr/include/$(HEADERS_OUT_DIR)/$(hgf) $(FSDEV)/usr/include/$(HEADERS_OUT_DIR)/$(hod)/$(hgf); ))"
endif


#copy stuff to $(FS)/$(TARG_DIR) etc. here
EXEC_TNAMES = $(notdir $(EXECS))
DYN_TNAMES = $(notdir $(DYNS))
STATIC_TNAMES = $(notdir $(STATICS))

EXEC_FNAMES = $(addprefix $(FS)/$(TARG_DIR)/, $(EXEC_TNAMES))
DYN_FNAMES = $(addprefix $(FS)/usr/lib/,$(DYN_TNAMES)) $(addprefix $(FSDEV)/usr/lib/,$(DYN_TNAMES))
STATIC_FNAMES = $(addprefix $(FSDEV)/usr/lib/, $(STATIC_TNAMES))
HEADER_FNAMES = $(addprefix $(FSDEV)/usr/include/$(strip $(HEADERS_OUT_DIR)), $(notdir $(HEADER_FILES)))


RM_CMD = $(INS_PFX) rm
RM_POST = 2>/dev/null || true
uninstall::
#$(INS_PFX) rm $(EXEC_FNAMES)
	@$(RM_CMD) $(EXEC_FNAMES) $(RM_POST)
	@$(RM_CMD) $(DYN_FNAMES) $(RM_POST)
	@$(RM_CMD) $(STATIC_FNAMES) $(RM_POST)
	@$(RM_CMD) $(HEADER_FNAMES) $(RM_POST)
	
ifneq ($(strip $(DYNS)),)
$(OUT_DIR)/.fake.so: | $(OUT_DIR)
	@touch $(OUT_DIR)/.fake.cpp
	@$(CXX) -fPIC -c $(OUT_DIR)/.fake.cpp -o $(OUT_DIR)/.fake.o
	@$(CXX) -Wall -shared -fPIC -o $@ $(OUT_DIR)/.fake.o

preinstall:: $(OUT_DIR) $(OUT_DIR)/.fake.so 
	$(foreach dyn,$(strip $(DYN_FNAMES)), $(shell $(INS_PFX) cp $(OUT_DIR)/.fake.so $(dyn)))
endif
preinstall:: 
ifneq ($(strip $(HEADER_FILES)),)
	@$(INS_PFX) mkdir -p $(FSDEV)/usr/include/$(HEADERS_OUT_DIR)/ $(addprefix $(FSDEV)/usr/include/$(HEADERS_OUT_DIR)/,$(HEADER_CP_DIRS))
	@$(INS_PFX) cp $(HEADER_FILES) $(FSDEV)/usr/include/$(HEADERS_OUT_DIR)/
endif
ifneq ($(strip $(HEADER_CP_DIRS)),)
	@$(INS_PFX) cp -r $(HEADER_CP_DIRS) $(FSDEV)/usr/include/$(HEADERS_OUT_DIR)/
endif

autorun:: 
	
	
disable:: 
	

strip:: $(EXECS) 
#strip -s -R .comment -R .gnu.version --strip-unneeded $(EXECS)
	
scan :: clean 
	scan-build make 2>&1
	
docs:: 
	doxygen

docs-install:: docs 

vcs-refresh-ignores: 
#$(foreach fname,$(strip $(shell svn propget svn:ignore | tr "\n" " " || true)),$(call ignore,$(fname)))
#@svn propset svn:ignore -F .svnignore . > /dev/null 2>/dev/null

	
#CULL_FILES=$(strip $(shell svn st | grep ^! | awk '{$$1=""; print "\""substr($$0,2)"\"" }' | xargs))
vcs-new-files::
	@find . -type d -empty -not -path "./.git/*" -exec touch {}/.gitkeep \;
	$(eval SRCS_GREP_PATT:=$(shell git ls-files --others --exclude-standard --modified | tr "\n" "|" | sed 's/|$$//'))
	$(eval SRCS_NEW:=$(shell echo "$(value ALL_SRCS)" | tr " " "\n" | uniq | grep -E "$(SRCS_GREP_PATT)" | uniq | tr "\n" " "))
	$(eval ALL_SRCS +=$(shell cat $$(find . -type f -name '*.d' -exec echo {} + 2>/dev/null) | grep ".h:" | grep -v "^/" | tr ":\n" " "))
	@test -z "$(strip $(value SRCS_NEW))" || git add $(value SRCS_NEW)
#@svn $(VCS_PRE_ARGS) add --parents $(SRCS_NEW) > /dev/null 2>/dev/null || true
vcs-cull-files:
	@git rm --cached `git ls-files -i --exclude-from=.gitignore` >/dev/null 2>&1 || true
#@svn delete --force --keep-local `svn pg  svn:ignore . | tr "\n" " " | xargs` >/dev/null 2>/dev/null || true
#@$(foreach fname,$(CULL_FILES),svn rm --force --keep-local $(strip $(fname)) 2>/dev/null || true ) 
#@svn st | grep ^! | awk '{$$1=""; print " --force --keep-local \""substr($$0,2)"\"" }' | xargs svn rm >/dev/null 2>/dev/null|| true

ifdef COMMIT_MSG
VCS_COMMIT_EX +=-m "$(COMMIT_MSG)"
endif

commit: .lpbmeta vcs-refresh-ignores vcs-new-files vcs-cull-files
	@git $(VCS_CM_ARGS) commit -qa $(VCS_COMMIT_EX) 2>/dev/null || true
update:
	@git $(VCS_PRE_ARGS) pull 2>/dev/null || true

	
push: .lpbmeta
	@echo "Pushing to $$(git config --get remote.origin.url)"
	-@git config --global credential.helper store
	@git $(VCS_PRE_ARGS) push || true
pull: update
	
status: .lpbmeta vcs-refresh-ignores vcs-new-files vcs-cull-files
	$(eval VCS_STAT:=)
	$(eval DEL_F :=$(shell git diff --cached --name-only --diff-filter=D | xargs echo))
	$(eval MOD_F :=$(shell echo "$(strip $(shell git diff --cached --name-only --diff-filter=ad | xargs echo) $(shell git diff --name-only --diff-filter=ad | xargs echo))" | tr ' ' '\n' | sort | uniq | xargs echo))
	$(eval ADD_F :=$(shell git diff --cached --name-only --diff-filter=A | xargs echo))
	@printf "[$(words $(DEL_F))] Deleted: $$(tput bold)$$(tput setaf 1)\n$(DEL_F:%=\t- %\n)$$(tput sgr0)"
	@printf "[$(words $(MOD_F))] Modified: $$(tput bold)$$(tput setaf 6)\n$(MOD_F:%=\t~ %\n)$$(tput sgr0)"
	@printf "[$(words $(ADD_F))] Added: $$(tput bold)$$(tput setaf 2)\n$(ADD_F:%=\t+ %\n)$$(tput sgr0)"
	
#@(printf "`git ls-files --deleted --modified`\n`git diff --cached --name-only --diff-filter=A`") | sort | uniq | xargs echo

vcs-edit-ignores: .lpbmeta
	@$(firstword $(strip $(strip $(shell echo $$EDITOR)) nano)) .gitignore
	
ignore: vcs-edit-ignores vcs-refresh-ignores vcs-cull-files

add: vcs-new-files
	$(eval ADD_F :=$(shell git diff --cached --name-only --diff-filter=A | xargs echo))
	@printf "[$(words $(ADD_F))] Added: $$(tput bold)$$(tput setaf 2)\n$(ADD_F:%=\t+ %\n)$$(tput sgr0)"
	
#@svn propset svn:ignore "./$(BUILD_DIR)" .
#	svn $(VCS_CM_ARGS) add --force . --auto-props --parents --depth infinity -q
#@svn $(VCS_CM_ARGS) rm $(BUILD_DIR)/* --keep-local
#@svn $(VCS_CM_ARGS) rm $(EXEC_TNAMES) $(DYN_TNAMES) $(STATIC_TNAMES) --keep-local
	
metadata:: saveMetadata

-l%::|lib%.so
	

.FORCE: FORCE
FORCE:
