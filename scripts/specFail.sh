#!/bin/bash
args=("$@")

spec=${args[0]}
platform=${args[1]}
failCode=${args[2]}
log=${args[3]}

echo "SPECFAIL: ${spec} Platform:${platform} code:${failCode} Log:${log}"
