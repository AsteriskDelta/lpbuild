#Template target file, T_NAME should match SYS_NAME in the target Makefile
T_NAME := DeltaIIITech
T_DIR_NAME := DeltaIII.tech
#T_OWNERS += 

#T_VCS_PATH = https://github.com/AsteriskDelta/deltaiii.tech.git
T_VCS_PATH = https://bitbucket.org/AsteriskDelta/deltaiii.tech.git

default: $(T_NAME)
TARGET_DIR = targets
include targets/Targets.mk

$(T_NAME): $(T_DEST)
	+@$(SUBMAKE_PREFIX) $(MAKE) -C $(T_DEST) $(MK_ARGS) $(CHILD_ACT_TARGETS)
	@$(TARGET_MAIN_POST)



