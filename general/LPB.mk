export
.PHONY: default actual actual_pre actual_post targets FORCE loadMetadata saveMetadata prepareMetadata doActivate doDeactivate buildsysUpdate

COMMA:=,
SPACE:= 
NEWLINE:=$(shell printf "\n")

ifeq ($(BUILDSYS_INTERNAL),true)
INSTANCE_BSID = $(shell cat .bsid 2> /dev/null || ((echo -n "$(shell pwd)" | md5sum | awk '{print $$1}') > .bsid && cat .bsid))
else 
ifndef NAME
overrideDefErr: 
.PHONY: overrideDefErr
$(error No NAME variable declared in $(shell realpath `pwd`)/$(firstword $(MAKEFILE_LIST))- define this before including any build system Makefiles! Bailing out...)
endif
#BUILDSYS_PARENT = $(shell cat .bsparent 2> /dev/null || echo "WARNING: No parent buildsys directory detected, dependancies and activate/deactivate targets are disabled!")
BUILDSYS_REPO_LOCAL ?= .
INSTANCE_BSID :=$(strip $(shell cat $(BUILDSYS_REPO_LOCAL)/.bsid 2> /dev/null))
ifeq ($(INSTANCE_BSID),)
$(info WARNING: Not attached to any build system, run 'make activate' to enable full functionality)
endif
ifndef SYS_NAME
SYS_NAME = $(shell echo $(NAME) | tr A-Z a-z)
#$(info Warning: SYS_NAME is undefined, automatically setting it to "$(SYS_NAME)"; if this is incorrect, set it yourself!)
endif
ifndef TARGET_PLATFORM
TARGET_PLATFORM = Desktop
#$(info Warning: TARGET_PLATFORM undefined, assuming Desktop; if this is incorrect, set it in your env or on the command line!)
endif

endif

ifneq ($(shell whoami),root)
ifeq ($(TARGET_PLATFORM),Desktop)
INS_PFX = sudo
endif
endif
INS_PFX ?=

#TARGETS = 
#PRE_TARGETS =
#POST_TARGETS = 

#VERSION ?= trunk
USER_DIR ?= /home/$(USER)
METAD_FOLDER = $(USER_DIR)/.lpb/$(INSTANCE_BSID)
METAD_PACK_NAME = meta.mk
PRE_TARGETS += loadMetadata
#POST_TARGETS += saveMetadata

#MAKE_PID := $(shell echo $$PPID)
CORES = $(shell nproc --all)
#JOB_FLAG := $(filter -j%, $(subst -j ,-j,$(shell ps T | grep "^\s*$(MAKE_PID).*$(MAKE)")))
#JOBS     := $(subst -j,,$(JOB_FLAG))
#JOBS := $(CORES)
#JOB_FLAG := -j$(CORES) 

METAD_FULL := $(METAD_FOLDER)/$(METAD_PACK_NAME)
METAD_INCLUDE := $(METAD_FOLDER)/fakeroot
AUX_INCLUDE_PATH := -I$(METAD_INCLUDE)/usr/include

VCS_SOURCE := $(strip $(shell test -f .git && basename `git rev-parse --show-toplevel`))
VCS_REV := $(strip $(shell test -f .git && git log --pretty=format:'%h' -n 1))
ifndef BUILDSYS_INTERNAL
ifndef BUILDSYS_NO_VCS
DEF_VCS_FLAGS ?=
ifneq (,$(VCS_SOURCE))
DEF_VCS_FLAGS += -DVCS_SOURCE="\"$(VCS_SOURCE)\""
endif
ifneq (,$(VCS_REV))
DEF_VCS_FLAGS += -DVCS_REV="\"$(VCS_REV)\""
endif
CFLAGS +=$(DEF_VCS_FLAGS) 
LFLAGS +=$(DEF_VCS_FLAGS) 
endif
endif

ifneq (,$(findstring release,$(MAKECMDGOALS)))
BS_COMPILE_MODE = release
else
BS_COMPILE_MODE = debug
endif

ifneq ($(BUILDSYS_INTERNAL),true)
ifneq ($(INSTANCE_BSID),)
PLATFORM_DIR = $(strip $(shell readlink $(METAD_FOLDER)/buildsys))/platforms
include $(PLATFORM_DIR)/$(TARGET_PLATFORM).mk
ifndef BUILDSYS_AUTO
$(info Using platform information at $(PLATFORM_DIR)/$(TARGET_PLATFORM).mk)
endif
endif
endif

SUBMAKE_PREFIX ?= 
silent:
	@
.PHONY: silent
	
default: $(LL_TARGETS)
-include $(METAD_FULL)

actual_pre: $(PRE_TARGETS)
#@echo "ACTUAL PRE"
	
actual_post: $(POST_TARGETS)
#@echo "ACTUAL POST"
	
FORCE:

actual: actual_pre targets actual_post

doNothing:
	
refreshMetadata: actual_pre actual_post
	
prepareMetadata:
ifneq ($(INSTANCE_BSID),)
	@mkdir -p $(METAD_FOLDER)
ifeq ($(BUILDSYS_INTERNAL),true)
	@(rm $(METAD_FOLDER)/buildsys || true) 2> /dev/null
	@ln -s $(shell pwd) $(METAD_FOLDER)/buildsys
endif
endif
##touch $(METAD_FOLDER)/$(METAD_PACK_NAME)
	
loadMetadata: prepareMetadata

LCL_PATH_VAR:=MEXP_PATH_$(NAME)_$(VERSION)
LCSYS_PATH_VAR:=MEXP_PATH_$(SYS_NAME)_$(VERSION)

#export MEXP_* variables, crazy double-sed pipe is to convert bash->makefile and unescape for metaprogramming
saveMetadata:
ifneq ($(INSTANCE_BSID),)
#@echo "Saving metadata to $(METAD_FULL)..."
#@echo "test var = $(MEXP_TEST)"
#export
#@(export | grep "export MEXP_" | sed 's/export //g' | sed -r 's/\\(.)/\1/g' | sed 's/"//g') > $(METAD_FULL)
#echo "$$(cat $(METAD_FULL) 2>/dev/null || echo '')"
	@printf "$(LCL_PATH_VAR):=$(shell pwd)\n$$( (cat $(METAD_FULL) 2>/dev/null || echo '') | grep -v '$(LCL_PATH_VAR)'| sed '/^$$/d')\n" > $(METAD_FULL)
	@printf "$(LCSYS_PATH_VAR):=$(shell pwd)\n$$( (cat $(METAD_FULL) 2>/dev/null || echo '') | grep -v '$(LCSYS_PATH_VAR)'| sed '/^$$/d')\n" > $(METAD_FULL)
#

ACTIVATE_DEPS:=doDeactivate
endif
	
doActivate: $(ACTIVATE_DEPS)
	$(eval $(LCL_PATH_VAR) := $(shell pwd))
	$(eval BS_INSTANCES:=$(shell cd $(USER_DIR)/.lpb; find . -maxdepth 1 -type d -print | sed 's/.\///g' | sed '/^\./ d' | tr "\n" " "))
	$(eval BS_PATHS:=$(foreach bID,$(BS_INSTANCES),$(shell readlink $(METAD_FOLDER)/$(bID)/buildsys)))
	$(eval BS_UIIDS:=$(shell seq 1 $(words $(strip $(BS_INSTANCES))) | tr "\n" " "))
	@echo "Local build system instances:"
	@printf "$$(printf "ID $(BS_UIIDS)\nPath $(BS_PATHS)\nUUID $(BS_INSTANCES)\n" | transp | column -t | sed 's/^/\t/')\n"
	@printf "Which build system should this attach to? [ID]: "
	@read cin; (echo "$(BS_INSTANCES)" | cut -d " " -f $$cin) > .bsid;\
	printf "Using build system at: $$(echo "$(BS_PATHS)" | cut -d " " -f $$cin)\n\tUUID = `cat .bsid`\n";
	+@$(MAKE) saveMetadata --no-print-directory
#@echo "Set $(LCL_PATH_VAR) to $($(LCL_PATH_VAR))"
activate: actual_pre doActivate actual_post	

doDeactivate:
ifneq ($(INSTANCE_BSID),)
	@echo "$$((cat $(METAD_FULL) 2>/dev/null || echo '') | grep -v '$(LCL_PATH_VAR)' | sed '/^$$/d')" > $(METAD_FULL);
	@rm .bsid 2>/dev/null || true
	@echo "Cleared $(LCL_PATH_VAR) and LPB instance"
else
	@echo "Not attached to any build systems, nothing to do."
endif
deactivate: actual_pre doDeactivate actual_post
	
#> $(METAD_FULL)
#post_force: .FORCE $(MAKECMDGOALS) actual_post
ifndef BUILDSYS_INTERNAL

VCS_PRE_ARGS=

ifndef TEMPLATED_USED
metadata:: saveMetadata
status::
push::
pull::
verbose::
uninstall::	
install::

commit::
	@git commit -a

update:
	@git $(VCS_PRE_ARGS) pull
endif

deps:
	@make -C `readlink $(METAD_FOLDER)/buildsys` $(TARGET_PLATFORM) $(SYS_NAME).deps do $(BS_COMPILE_MODE) preinstall
	
endif
.FORCE:
