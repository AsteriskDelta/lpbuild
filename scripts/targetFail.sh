#!/bin/bash
args=("$@")

target=${args[0]}
version=${args[1]}
platform=${args[2]}
failCode=${args[3]}
logPath=${args[4]}
ownerPath=${args[5]}

echo "TARGETFAIL: ${target}:${version} Platform:${platform} code:${failCode} Log:${logPath} OwnerPath:${ownerPath}"
