include $(PLATFORM_DIR)/Platform.mk

SYS_PATH = /
CXX = g++
CC = gcc
FS = /
FSDEV = /
QMAKE = qmake
LD = ld
MAKE = make
QMAKE_SPEC = linux-g++

MI_PATH = /opt/MI/

SYS_CODE = Desktop
SUBMAKE_PREFIX += TARGET_PLATFORM=Desktop 
PLATFORM_TARGET =
TARGET_PLATFORM=Desktop
