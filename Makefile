default: build | silent
BUILDSYS_INTERNAL = true
BUILDSYS_AUTO=true
REC_ID ?=0

ARCHIVE_DIR ?= $(dir $(shell pwd)/)archives/
PLATFORM_DIR = platforms
TARGET_DIR = targets
SPEC_DIR = specs
PROXY_DIR = proxies
ifneq (,$(findstring auto,$(MAKECMDGOALS)))
BUILDS_DIR :=$(strip $(shell realpath $(firstword $(strip $(shell cat .bdir 2>/dev/null || (bdir="builds";\
echo -n "$$bdir" > .bdir && cat .bdir))) builds)))
else
BUILDS_DIR :=$(strip $(shell realpath $(firstword $(strip $(shell cat .bdir 2>/dev/null || (read -p "Where should repositories be put? [leave blank for ./builds]: " bdir;\
if [[ -z $$bdir ]]; then bdir="builds"; fi;\
echo -n "$$bdir" > .bdir && cat .bdir))) builds)))
endif

FS = test
CHILD_TARGETS = 
ifneq (,$(findstring actual,$(MAKECMDGOALS)))
# Found
else
    LL_TARGETS += clearSpec build
endif

MK_ARGS = $(JOB_FLAG)
include general/LPB.mk

#.EXPORT_ALL_VARIABLES:
FORCE:

buildsysUpdate: | silent
	@echo "Updating Buildsys local copy..."
	@git pull || true
	
buildsysCommit: | silent
	@echo "Committing Buildsys local copy..."
ifdef COMMIT_MSG
	-@git commit -a -m "$(COMMIT_MSG)"
else
	-@git commit -a
endif

buildsysPush: | silent
	@echo "Commiting LPG root local copy..."
	-@git push

TSC:=.tspec$(REC_ID).mk
RSC:=.rspec$(REC_ID).mk
PSC:=.pspec$(REC_ID).mk
	
clearSpec: | silent
	@rm $(TSC) 2>/dev/null || true
	@rm $(RSC) 2>/dev/null || true
	@rm $(PSC) 2>/dev/null || true
.PHONY: clearSpec

clearAllSpec: | silent
	@rm .*.mk 2>/dev/null || true
.PHONY: clearAllSpec

%: $(PLATFORM_DIR)/%.mk clearSpec .FORCE | silent
	@cat $(PLATFORM_DIR)/$@.mk >> $(PSC)
	
%: $(SPEC_DIR)/%.mk clearSpec .FORCE | silent
	@cat $(SPEC_DIR)/$@.mk >> $(RSC)
	@echo "TARGET_SPECS += $@" >> $(RSC)
	
%.deps: $(TARGET_DIR)/%.mk.deps clearSpec .FORCE | silent
	@cat $(TARGET_DIR)/$(subst .deps,,$@).mk.deps >> $(TSC)
	
%: $(PROXY_DIR)/%.mk clearSpec .FORCE | silent
	@cat $(PROXY_DIR)/$@.mk >> $(PSC)
	@echo "TARGET_PROXIES += $@" >> $(PSC)

#null pipe to prevent "up to date" messages from gmake
%: $(TARGET_DIR)/%.mk .FORCE | silent
#@echo "Adding $@" > /dev/null
	$(eval TARGETS += $@)


$(TSC): | silent
$(PSC): | silent
$(RSC): | silent
ifneq (,$(findstring actual,$(MAKECMDGOALS)))
-include $(TSC)
-include $(RSC)
-include $(PSC)
endif
#-include $(TARGET_DIR)/*.mk


dbgPrint: | silent
	@echo "TARGETS = $(TARGETS)"
	@echo "CHILD_TARGETS = $(CHILD_TARGETS)"
POST_TARGETS += printComplete
#dbgPrint

#Dirty hack to force the construction of recursion files prior to actual recursion- makefiles rearrange targets (without explicit dependancies) as they please
ifneq (,$(findstring actual,$(MAKECMDGOALS)))
build: | silent
else 
build: clearSpec $(filter-out do,$(filter-out build,$(MAKECMDGOALS))) | silent
	@echo "Build system at revision #$(VCS_REV), using BSID #$(INSTANCE_BSID) at recursion level $(REC_ID)"
#unset TARGETS ;\
#unset LL_TARGETS ;\
#unset PRE_TARGETS ;\
#unset POST_TARGETS ;
	@$(MAKE) --no-print-directory -f Makefile actual $(MK_ARGS)
endif
do:build | silent
	
$(TARGETS): %: $(TARGET_DIR)/%.mk | silent
#@echo "Target: $< of {$(TARGETS)}"
#@echo "Child targets: $(CHILD_TARGETS)"
#@echo "(MAKE) -f $< $(MK_ARGS) CHILD_TARGETS=\"$(CHILD_TARGETS)"\"
	@unset TARGETS ;\
	unset LL_TARGETS ;\
	unset PRE_TARGETS ;\
	unset POST_TARGETS ;\
	unset BUILDSYS_INTERNAL;\
	$(MAKE) -f $< $(MK_ARGS) CHILD_TARGETS="$(CHILD_TARGETS)"

postTarg:

FMT_TARGETS=$(subst $(SPACE),$(COMMA)$(SPACE),$(strip $(TARGETS)))
preprint: | silent
	@echo "Building [$(strip $(CHILD_TARGETS))] for $(FMT_TARGETS:%$(COMMA)=%)"
#@echo "{preprint $(filter-out $(SKIP), $(EX_TARGETS) $(TARGETS)) postTarg}"
	
targets: preprint $(filter-out $(SKIP), $(EX_TARGETS) $(TARGETS)) postTarg | silent

ifneq (,$(findstring actual,$(MAKECMDGOALS)))
POST_TARGETS += printComplete
else
.PHONY: build do
endif
POST_TARGETS += clearAllSpec | silent

sysinstall:
	sudo cp general/LPB.mk /usr/include/
	sudo cp general/Template.mk /usr/include/LPBT.mk
	sudo cp general/transp /usr/bin/transp
	sudo chmod +x /usr/bin/transp
	
printComplete: | silent
	@printf "\nAll targets ($(TARGETS)) were completed\n"
#.SECONDEXPANSION:
define recurse =
	sudo -u $(USER) -i bash -l -c "cd $(PWD); REC_ID=$$(($(REC_ID) + 1)) $(SUBMAKE_PREFIX) $(MAKE) --no-print-directory -f Makefile $(TARGET_PLATFORM) $(TARGET_SPECS) do $1" >&2
endef

recurse_%: .FORCE | silent
	@$(call recurse,$(subst _,$(SPACE),$*))
